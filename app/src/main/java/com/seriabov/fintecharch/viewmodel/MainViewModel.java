package com.seriabov.fintecharch.viewmodel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import com.seriabov.fintecharch.model.CoinInfo;
import com.seriabov.fintecharch.service.AppDelegate;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainViewModel extends ViewModel
{
    private  MutableLiveData<Boolean> loadingData = new MutableLiveData<>();
    private  MutableLiveData<Boolean> isError = new MutableLiveData<>();
    private  MutableLiveData<List<CoinInfo>> coinsList = new MutableLiveData<>();

    public LiveData<Boolean> getLoading() {
        if (loadingData == null) {
            loadingData = new MutableLiveData<>();
        }
        return loadingData;
    }

    public LiveData<Boolean> getError() {
        if (isError == null) {
            isError = new MutableLiveData<>();
        }
        return isError;
    }

    public LiveData<List<CoinInfo>> getData() {
        if (coinsList == null) {
            coinsList = new MutableLiveData<>();
            requestNewData();
        }
        return coinsList;
    }

    public void requestNewData() {
        loadingData.setValue(true);
        isError.setValue(false);
        AppDelegate.getInstance().getApiService()
                .getCoinsList()
                .enqueue(new Callback<List<CoinInfo>>() {
                    @Override
                    public void onResponse(Call<List<CoinInfo>> call, Response<List<CoinInfo>> response) {
                        coinsList.setValue(response.body());
                    }
                    @Override
                    public void onFailure(Call<List<CoinInfo>> call, Throwable t) {
                        isError.setValue(true);
                    }
                });
    }
}
